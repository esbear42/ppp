#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>

double density (double x, void * params){
	double alpha = *(double*) params;
	double density = exp(-alpha*x*x);
	return density;
}

double energy (double x, void * params){
	double alpha = *(double*) params;
	double energy = 0.5*(x*x+alpha-alpha*alpha*x*x)*exp(-alpha*x*x);
	return energy;
}

int main (void){
	gsl_integration_workspace *w = gsl_integration_workspace_alloc (1000);
	
	double Dresult, Derror;
	double Eresult, Eerror;
	
	printf("%s\t%s\n", "alpha", "E");
	
	for(double alpha = 0.5, s=0.01;alpha<2;alpha=alpha+s){
	
	gsl_function  D;
	D.function = &density;
	D.params = &alpha;
	
	gsl_function E;
	E.function = &energy;
	E.params = &alpha;
	
	gsl_integration_qagi (&D, 0, 1e-7, 1000, w, &Dresult, &Derror);
	gsl_integration_qagi (&E, 0, 1e-7, 1000, w, &Eresult, &Eerror);
	
	printf("%g\t%g\n", alpha, Eresult/Dresult);
}
	return 0;
}