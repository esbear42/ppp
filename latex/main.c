#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<getopt.h>

int diffeq (double x, const double u[], double dudx[], void * params){
	dudx[0] = 2/pow(M_PI,0.5)*exp(-x*x);
	return GSL_SUCCESS;
}

int main(int argc, char** argv){
	
		double xstart = atof(argv[1]);
		double xend = atof(argv[2]);
		double dx = atof(argv[3]);
	

	double u[]={0};

	gsl_odeiv2_system diffsys;
	diffsys.function = diffeq;
	diffsys.jacobian = NULL;
	
	
	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	
	
	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new
			(&diffsys, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);
	
	
	
	for(double xi = xstart, x=xstart; xi<xend;xi=xi+dx){
		int status = gsl_odeiv2_driver_apply(driver, &x, xi, u);
		printf("%g\t%g\n", xi, u[0]);
		if (status != GSL_SUCCESS) fprintf(stderr, "fun: status = %i", status);
	}
	
	
	gsl_odeiv2_driver_free (driver);
	
	return 0;
}