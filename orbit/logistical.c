#include<stdio.h>
#include<math.h>
#include<getopt.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>


int equation (double x, const double y[], double dydx[], void *params){
	dydx[0] = y[0]*(1-y[0]);
	return GSL_SUCCESS;
}

int main(int argc, char** argv){
	
	double xstart = 0, Dx = 0.01, xend = 10, y[] = {0.5};
	
	
	
	while(1){
		int opt = getopt(argc, argv, "l:h:");
		if(opt == -1) break;
		switch(opt){
			case 'l': xstart = atof(optarg); break;
			
			case 'h': xend = atof(optarg); break;
			
			default:
				fprintf(stderr, "Usage: %s [-e epsilon] [-p du]\n", argv[0]);
				exit(EXIT_FAILURE);
		}
	}
	
	double x = xstart;
	
	gsl_odeiv2_system logistical;
	logistical.function = equation;
	logistical.jacobian = NULL;
	logistical.dimension = 1;
	
	
	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	
	gsl_odeiv2_driver *driver 
		= gsl_odeiv2_driver_alloc_y_new
		(&logistical, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);	
	

	
	for (double xt=xstart; xt<xend;xt=xt+Dx){
		
		int status = gsl_odeiv2_driver_apply (driver, &x, xt, y);
		
		if (status != GSL_SUCCESS){
			printf("error");
		}
	
		printf("%g\t%g\n",x,y[0]);
	}
	return 0;
}
	