#include<stdio.h>
#include<gsl/gsl_errno.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_vector.h>
#include<assert.h>
#include<gsl/gsl_multiroots.h>


int diffeq (double r, const double y[], double dy[], void*params){
	double E = *(double*) params;
	dy[0] = y[1];
	dy[1] = 2*(-E*y[0]-y[0]/r);
	return GSL_SUCCESS;
}


double shooter (double E, double rmax, double dr){
	
	gsl_odeiv2_system hydrogen;
	hydrogen.function = diffeq;
	hydrogen.jacobian = NULL;
	hydrogen.dimension = 2;
	hydrogen.params = (void *) &E;
	
	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	
	
	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new
		(&hydrogen, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);
	
	double r = dr;
	double y[] = {r-r*r , 1-2*r};

	int status = gsl_odeiv2_driver_apply (driver, &r, rmax, y);
	
	return y[0];	
}

double bettershooter (double E, double rmax, double dr){
	
	gsl_odeiv2_system hydrogen;
	hydrogen.function = diffeq;
	hydrogen.jacobian = NULL;
	hydrogen.dimension = 2;
	hydrogen.params = (void *) &E;
	
	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	
	
	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new
		(&hydrogen, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);
	
	double r = dr;
	double k = pow(-2*E,0.5);
	double y[] = {r*exp(-k*r),(1-k*r)*exp(-k*r)};

	int status = gsl_odeiv2_driver_apply (driver, &r, rmax, y);
	
	return y[0];	
}

int master (const gsl_vector *e, void *params, gsl_vector *f){
	double E = gsl_vector_get(e,0);
	assert(E<0);
	double rmax = *(double*)params;
	double fval = shooter(E,rmax,0.01);
	gsl_vector_set(f,0,fval);
	return GSL_SUCCESS;
}

int bettermaster(const gsl_vector *e, void *params, gsl_vector *f){
	double E = gsl_vector_get(e,0);
	assert(E<0);
	double rmax = *(double*)params;
	double fval = bettershooter(E,rmax,0.01);
	gsl_vector_set(f,0,fval);
	return GSL_SUCCESS;
}

double rootsolver (double rmax, int better){
	
	gsl_multiroot_fsolver *solver =
		gsl_multiroot_fsolver_alloc (gsl_multiroot_fsolver_hybrids,1);
	
	gsl_multiroot_function func;
	if (better !=0){
		func.f = bettermaster;
	}
	else{
		func.f = master;
	}
	func.n = 1;
	func.params = (void*)&rmax;
	
	gsl_vector *e = gsl_vector_alloc(1);
	gsl_vector_set(e,0,-1);
	
	gsl_multiroot_fsolver_set(solver, &func, e);
	
	int status, iter = 0;
	const double epsabs = 1e-5;
	
	do{
		iter++;
		status = gsl_multiroot_fsolver_iterate(solver);
		if (status)break;
		status = gsl_multiroot_test_residual(solver->f, epsabs);		
		
	}while(status == GSL_CONTINUE && iter <100);
	
	return gsl_vector_get(solver->x,0);
}

void printwave (double E, double rmax, double dr, int better){
	
		for(double r=dr;r<rmax;r=r+dr){
		fprintf(stderr,"%g\t",r);
		fprintf(stderr,"%g\n",shooter(E,r,dr));
	}
}


int main (void){
	
	double rmaxmax = 12;
	
	printf("rmax\tE simpel\tE improved\n");
	
	for (double rmax = 8, drmax = 0.1; rmax < rmaxmax; rmax = rmax+drmax){
		printf("%g\t%.8g\t%.8g\n",rmax,rootsolver(rmax,0),rootsolver(rmax,1));
	}
	
	double rmax = 8, dr = 0.01;
	
	for (double r = 2*dr; r < rmax; r = r+dr){
		fprintf(stderr,"%g\t", r);
		fprintf(stderr,"%.8g\t", shooter(rootsolver(rmax,0),r,dr));
		fprintf(stderr,"%.8g\t", bettershooter( rootsolver(rmax,1),r,dr));
		fprintf(stderr,"%.8g\t", r*exp(-r));
		fprintf(stderr,"\n");
	}
	
	return 0;
}