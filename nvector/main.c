#include<stdio.h>
#include"nvector.h"
#include<stdlib.h>
#include<math.h>
#define RND (double)rand()/RAND_MAX

int main(void){

int n = 8;

printf("\nmain: testing nvector_alloc ... \n");
nvector *v = nvector_alloc(n);
if (v!=NULL) printf("Test went well :)\n");
else printf("Test failed :(\n");

printf("\nmain: testing nvector_set and nvector_get ...\n");
double value = 5;
int i = 0;
nvector_set(v,i,value);
double* vi = (double*)nvector_get(v,i);
if(double_equal(*vi,value)){printf("Test went well :)\n");}
else printf("Damnation! test failed :(\n");




printf("\nmain testing nvector_add ...\n");
nvector *a = nvector_alloc(n);
nvector *b = nvector_alloc(n);
nvector *c = nvector_alloc(n);
for (int i = 0; i < n; i++){
	double x = RND, y = RND;
	nvector_set(a,i,x);
	nvector_set(b,i,y);
	nvector_set(c,i,x+y);
	}
nvector_add(a,b);
nvector_print("a+b should be:   ",c);
nvector_print("a+b actually is: ",a);

if (nvector_equal(c,a)) printf("Test went well :)\n");
else printf("Damnation! test failed :(\n");

printf("\nmain testing nvector_sub ...\n");
for (int i = 0; i < n; i++){
	double x = RND, y = RND;
	nvector_set(a,i,x);
	nvector_set(b,i,y);
	nvector_set(c,i,x-y);
	}
nvector_sub(a,b);
nvector_print("a-b should be:   ",c);
nvector_print("a-b actually is: ",a);

if (nvector_equal(c,a)) printf("Test went well :)\n");
else printf("Damnation! test failed :(\n");


printf("\nTesting nvector_dotp ...\n");

double s = 0;

for (int i = 0; i < n; i++){
	double x = RND;
	double y = RND;
	nvector_set(a,i,x);
	nvector_set(b,i,y);
	s = s+(x*y);
	}
printf("The dot product should be: %g\n",s);
printf("It actually is:            %g\n",nvector_dotp(a,b));

if (s==nvector_dotp(a,b)) {printf("Test went well :)\n");}
else {printf("Damnation! test failed :(\n");}

printf("\nTesting nvector_norm ...\n");
for (int i = 0; i<n; i++){
	double x = RND;
	nvector_set(a,i,x);
	}
if (double_equal(nvector_norm(a)*nvector_norm(a),nvector_dotp(a,a))){
	printf("Test went well :)\n");}
else printf("Damnation! test failed :(");


printf("\nTesting nvector_set_zero ...\n");
for (int i = 0; i<n; i++){
	double x = RND;
	nvector_set(a,i,x);
	}
nvector_set_zero(a);
if (double_equal(nvector_norm(a),0.0)){printf("Test went well :)\n");}
else {printf("Damnation! test failed :(\n");}




nvector_free(v);
nvector_free(a);
nvector_free(b);
nvector_free(c);

return 0;
}
