#include"komplex.h"
#include<math.h>
#include<stdio.h>

void k_print (char* s,komplex z){
printf(s);
if(z.re!=0 && z.im >= 0){
printf("%g+%gi\n",z.re,z.im);
}
else if(z.re != 0 && z.im < 0){
printf("%g-%gi\n",z.re,-z.im);
}
else if(z.im == 0){
printf("%g\n",z.re);
}
else{
printf("%gi\n",z.im);
};
};

void k_set (komplex* z, double x, double y){
z->re = x;
z->im = y;
};

komplex k_new(double x, double y){
komplex z;
z.re = x;
z.im = y;
return z;
};

komplex k_add(komplex a, komplex b){
komplex z;
z.re = a.re + b.re;
z.im = a.im + b.im;
return z;
};

komplex k_sub(komplex a, komplex b){
komplex z;
z.re = a.re - b.re;
z.im = a.im - b.im;
return z;
};

komplex k_inv(komplex a){
komplex z;
z.re = a.re/((a.re*a.re)+(a.im*a.im));
z.im = -a.im/((a.re*a.re)+(a.im*a.im));
return z;
};

komplex k_mul(komplex a, komplex b){
komplex z;
z.re = (a.re * b.re) - (a.im * b.im);
z.im = (a.re * b.im) + (a.im * b.re);
return z;
};

komplex k_div(komplex a, komplex b){
komplex z = k_mul(a,k_inv(b));
return z;
};


komplex k_conj(komplex z){
return k_new(z.re,-z.im);
};

double k_abs(komplex z){
return (pow((z.re*z.re)+(z.im*z.im),0.5));
};

komplex k_exp(komplex z){
return k_new(exp(z.re)*cos(z.im),exp(z.re)*sin(z.im));
};

komplex k_cos(komplex z){
komplex eiz = k_exp(k_mul(z,k_i));
komplex cosz = k_mul(k_new(0.5,0),(k_add(eiz,k_inv(eiz))));
return cosz;
};


komplex k_sin(komplex z){
komplex eiz = k_exp(k_mul(z,k_i));
komplex sinz = k_mul(k_new(0.5,0),(k_sub(k_inv(eiz),eiz)));
return sinz;
};

