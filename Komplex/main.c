#include<stdio.h>
#include"komplex.h"

int main(void){

komplex x = k_new(13,36);
komplex y = k_new(47,-16);

//k_set(&x,0,0);

printf("\nGiven two complex numbers:\n");
k_print("x = ",x);
k_print("y = ",y);

printf("\nTheir sum is:\n");
k_print("x+y = ",k_add(x,y));

printf("\nTheir difference is:\n");
k_print("x-y = ",k_sub(x,y));

printf("\nTheir product is:\n");
k_print("x*y = ",k_mul(x,y));

printf("\nTheir quotient is:\n");
k_print("x/y = ",k_div(x,y));

printf("\nThey have complex conjugates:\n");
k_print("x* = ",k_conj(x));
k_print("y* = ",k_conj(y));

printf("\nTheir absoulte values are:\n|x| = %g\n|y| = %g\n",k_abs(x),k_abs(y));

printf("\nUsing x and y, various usefull fubnctions have the values:\n");
k_print("exp(x) = ",k_exp(x));
k_print("exp(y) = ",k_exp(y));
k_print("sin(x) = ",k_sin(x));
k_print("sin(y) = ",k_sin(y));
k_print("cos(x) = ",k_cos(x));
k_print("cos(y) = ",k_cos(y));

return 0;
}
