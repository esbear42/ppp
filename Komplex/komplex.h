#ifndef HAVE_KOMPLEX_H

struct komplex {double re; double im;};
typedef struct komplex komplex;

#define k_i k_new(0,1)				//i as a komplex number.
#define k_1 k_new(1,0)				//1 as a komplex number.
#define k_pi k_new(M_PI,0)			//pi as a komplex number.
#define k_ipi k_new(0,M_PI)			//i pi as a komplex number.

void k_print(char* s, komplex z);		//Prints a string and the komplex number 
void k_set(komplex* z, double x, double y);	//Changes the value at the pointer
komplex k_new(double x, double y);		//Returns the complex number x+iy

komplex k_add(komplex a, komplex b);		//complex addition
komplex k_sub(komplex a, komplex b);		//complex subtraction
komplex k_mul(komplex a, komplex b);		//complex multiplication
komplex k_inv(komplex a);			//returns 1/a, used for division
komplex k_div(komplex a, komplex b);		//complex division

komplex k_conj(komplex z);			//Returns the complex conjugate
double k_abs(komplex z);			//Returns the absoulte value as a double

komplex k_exp(komplex z);			//The various functions on komplex data.
komplex k_sin(komplex z);
komplex k_cos(komplex z);



#define HAVE_KOMPLEX_H
#endif
