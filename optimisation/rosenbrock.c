#include<stdio.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>

double rosenbrock(const gsl_vector * r, void* params){
	double x = gsl_vector_get(r,0);
	double y = gsl_vector_get(r,1);
	double f = pow((1-x),2) + 100*pow((y-x*x),2);
	return f;
}

int main(void){
	
	gsl_multimin_function F;
	F.f = rosenbrock;
	F.n = 2;
	F.params = NULL;
	
	gsl_vector *start = gsl_vector_alloc(F.n);
	gsl_vector_set (start, 0,0);
	gsl_vector_set (start, 1,0);
	
	gsl_vector *step = gsl_vector_alloc(F.n);
	gsl_vector_set (step, 0,0.05);
	gsl_vector_set (step, 1,0.05);
	
	gsl_multimin_fminimizer *state =
		gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2, F.n);
	gsl_multimin_fminimizer_set (state, &F, start, step);
	
	int iter = 0, status;
	double acc = 0.001;
	fprintf(stderr, "iter\tx\ty\tsize\n");
	do{
		iter++;
		int iteration_status = gsl_multimin_fminimizer_iterate(state);
		if(iteration_status !=0){
			fprintf(stderr,"unable to improve\n");
			break;
		}
		
		status = gsl_multimin_test_size(state->size, acc);
		if(status == GSL_SUCCESS){
			fprintf(stderr, "converged :)\n");
		}

		
			fprintf(stderr,"%3i\t",iter);
			fprintf(stderr,"%.3f\t",gsl_vector_get(state->x,0));
			fprintf(stderr,"%.3f\t",gsl_vector_get(state->x,1));
			fprintf(stderr,"%.3f\n",state->size);
		
	}while(status == GSL_CONTINUE && iter < 100);

	printf("iter = %i\n",iter);
	printf("x = %.3f\n",gsl_vector_get(state->x,0));
	printf("y = %.3f\n",gsl_vector_get(state->x,1));
	printf("size = %.3f\n",state->size);
	
	return 0;
}