#include<stdio.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<assert.h>

double fit (double A, double B, double T, double t){
	assert(T!=0);
	return A*exp(-t/T)+B;
}

double sqerr (const gsl_vector *r, void* params){
	double A = gsl_vector_get(r,0);
	double B = gsl_vector_get(r,1);
	double T = gsl_vector_get(r,2);
	
	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);
	
	double sum;
	
	for (int i = 0; i < n; i++){
	sum = sum + pow(y[i]-fit(A,B,T,t[i]),2)*pow(e[i],-2);
	}
	return sum;
}



int main(void){
	
	gsl_multimin_function F;
	F.f = sqerr;
	F.n = 3;
	F.params = NULL;
	
	gsl_vector *start = gsl_vector_alloc(F.n);
	gsl_vector_set (start, 0, 5);
	gsl_vector_set (start, 1, 1.2);
	gsl_vector_set (start, 2, 2.5);
	
	gsl_vector *step = gsl_vector_alloc(F.n);
	gsl_vector_set (step, 0, 0.1);
	gsl_vector_set (step, 1, 0.1);
	gsl_vector_set (step, 2, 0.1);
	
	gsl_multimin_fminimizer *state = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2, F.n);
	gsl_multimin_fminimizer_set (state, &F, start, step);
	
	
	int iter = 0, status;
	double acc = 0.1;
	fprintf(stderr,"iter\tA\tB\tT\tsize");
	do{
		iter++;
		int istatus = gsl_multimin_fminimizer_iterate(state);
		
		status = gsl_multimin_test_size(state->size, acc);
		
		if(istatus != 0){
			fprintf(stderr, "unable to improve :(\n");
		}
		if(status == GSL_SUCCESS){
		fprintf(stderr,"converged:\n");
		}
		fprintf(stderr,"%3i\t",iter);
		fprintf(stderr,"%.3f\t",gsl_vector_get(state->x,0));
		fprintf(stderr,"%.3f\t",gsl_vector_get(state->x,1));
		fprintf(stderr,"%.3f\t",gsl_vector_get(state->x,2));
		fprintf(stderr,"%.3f\n",state->size);
		
	}while(status == GSL_CONTINUE && iter < 100);
	
	
	double A = gsl_vector_get(state->x,0);
	double B = gsl_vector_get(state->x,1);
	double T = gsl_vector_get(state->x,2);
	
	printf("t\ty\n");
	
	
	
	for(double tplot = 0, dtplot = 0.1; tplot<10;tplot = tplot+dtplot){
		printf("%.3f\t%.3f\n",tplot,fit(A,B,T,tplot));
	}
	
	return 0;
}