#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diffeq ( double x, const double y[], double dydx[], void*params){
	
	dydx[0] = -1/(x*x+1);
	
	return GSL_SUCCESS;
}

double arccotan(double xend){
	
	
	
	double x = 0, y[] = {M_PI*0.5};
	
	gsl_odeiv2_system arccot;
	arccot.function = diffeq;
	arccot.jacobian = NULL;
	arccot.dimension = 1;
	
	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	
	gsl_odeiv2_driver *driver = 
		gsl_odeiv2_driver_alloc_y_new (&arccot, gsl_odeiv2_step_rk8pd, hstart,epsrel,epsabs);
	
	int status = gsl_odeiv2_driver_apply (driver, &x, fabs(xend), y);
	
	if (status != GSL_SUCCESS){
		fprintf(stderr,"\nerror\n");
	}
	
	if (xend >= 0){
		return y[0];
	}
	else{
		return M_PI-1*y[0];
	}
}

double math_arccotan (double x){
	double acot = M_PI*0.5-atan(x);
	return acot;
}

int main (void){
	
	double xstart = -4, xend = 4, DX = 0.01;
	
	for (double x = xstart; x < xend; x = x+DX){
	
	printf("%08g\t%08g\t%08g\t%08g\n",x,arccotan(x),math_arccotan(x),arccotan(x)-math_arccotan(x));
	
	}
	
	return 0;
}