#include<stdio.h>
#include<math.h>
#include<gsl/gsl_eigen.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_linalg.h>


double harmonic(double x, double omega){
	return 0.5*x*x*omega;
	}


int main(void){
	int n = 150;
	
	double s = 1.0/(n+1);
	
	double omega = 1000;
	
	gsl_matrix *H3 = gsl_matrix_calloc(n,n);
	for (int i = 0; i<n-1; i++){
		gsl_matrix_set(H3,i,i,-2);
		gsl_matrix_set(H3,i,i+1,1);
		gsl_matrix_set(H3,i+1,i,1);
	};
	gsl_matrix_set(H3,n-1,n-1,-2);
	gsl_matrix_scale(H3,-0.5/(s*s));
	
	gsl_matrix *H5 = gsl_matrix_calloc(n,n);
	for (int i = 0; i<n-2; i++){
		gsl_matrix_set(H5,i,i,-5/2);
		gsl_matrix_set(H5,i,i+1,4/3);
		gsl_matrix_set(H5,i+1,i,4/3);
		gsl_matrix_set(H5,i,i+2,-1/12);
		gsl_matrix_set(H5,i+2,i,-1/12);
	}
	gsl_matrix_set(H5,n-1,n-1,-5/2);
	gsl_matrix_set(H5,n-1,n-2,4/3);
	gsl_matrix_set(H5,n-2,n-1,4/3);
	
	gsl_matrix_scale(H5,-0.5/(s*s));
	
	
	
	/*
	gsl_matrix *Vh = gsl_matrix_calloc(n,n);
	for (int i = 0; i<n-1; i++){
		gsl_matrix_set(Vh,i,i,harmonic((double)i*s-0.5,omega*(double)n));
	};
	
	gsl_matrix_add(H3,Vh);
	*/
	gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc (n);
	gsl_eigen_symmv_workspace *v = gsl_eigen_symmv_alloc (n);
	
	gsl_vector *eval3 = gsl_vector_alloc(n);
	gsl_matrix *evec3 = gsl_matrix_calloc(n,n);
	gsl_eigen_symmv(H3,eval3,evec3,w);
	
	gsl_eigen_symmv_sort(eval3,evec3,GSL_EIGEN_SORT_VAL_ASC);
	
	gsl_vector *eval5 = gsl_vector_alloc(n);
	gsl_matrix *evec5 = gsl_matrix_calloc(n,n);
	gsl_eigen_symmv(H5,eval5,evec5,v);
	
	gsl_eigen_symmv_sort(eval5,evec5,GSL_EIGEN_SORT_VAL_ASC);
	
	
	
	fprintf (fopen("out.welle.txt","w"),"i\texact\t3 point\n");
	for (int k=0; k<8;k++){
		double exact = 0.5*M_PI*M_PI*(k+1)*(k+1);
		double calc3 = gsl_vector_get(eval3,k);
		double calc5 = gsl_vector_get(eval5,k+1);
		fprintf(fopen("out.welle.txt","a"),"%i\t%g\t%g\t%g\n", k, exact, calc3, calc5);
	};
	
	gsl_matrix_scale(evec3,-1.0);
	
	fprintf(fopen("out.wellf.txt","w"),"i\tu\n");
	for (int i=0; i<n; i++){
		double u[] = {gsl_matrix_get(evec3,i,0),gsl_matrix_get(evec3,i,1),gsl_matrix_get(evec3,i,2)};;
		fprintf(fopen("out.wellf.txt","a"),"%g\t%g\t%g\t%g\n",(i+1)*s,u[0],u[1],u[2]);
	};
	
	return 0;
}