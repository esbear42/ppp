Given a matrix equation of the form: $\mat{A}\vec{x} = \vec{b}$

$$\begin{pmatrix}
6.13&-2.9&5.86&\\
8.08&-6.31&-3.89&\\
-4.36&1&0.19&\\
\end{pmatrix}
\begin{pmatrix} x_0 \\ x_1 \\ x_2 \end{pmatrix} = 
\begin{pmatrix}
6.23\\
5.37\\
2.29\\
\end{pmatrix}$$

The solution to the matrix equation is:

$$ \vec{x} = \begin{pmatrix}
-1.14\\
-2.83\\
0.851\\
\end{pmatrix}$$
