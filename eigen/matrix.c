#include<stdio.h>
//#include<gsl/gsl_linalg.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_eigen.h>
#include<math.h>

int main (void){
	
	gsl_matrix *A =  gsl_matrix_calloc(3,3);

	double A_entry[] = {6.13, -2.90, 5.86,
						8.08, -6.31, -3.89,
						-4.36, 1.00 ,0.19};

	for(int i=0; i<3;i++){
		for( int j = 0; j<3; j++){
			gsl_matrix_set(A,i,j,A_entry[3*i+j]);
		};
	};
	
	printf("Given a matrix equation of the form: $\\mat{A}\\vec{x} = \\vec{b}$\n");
	
	printf("\n$$\\begin{pmatrix}\n");
	
	for(int i=0;i<3;i++){
		for(int j=0; j<3;j++){
			printf("%.3g\&",gsl_matrix_get(A,i,j));
		};
		printf("\\\\\n");
	};
	
	printf("\\end{pmatrix}\n");
	printf("\\begin{pmatrix} x_0 \\\\ x_1 \\\\ x_2 \\end{pmatrix} = \n");
	
	gsl_vector *b = gsl_vector_calloc(3);

	double b_entry[] = {6.23, 5.37, 2.29};
	
	for (int i = 0; i<3;i++){
		gsl_vector_set(b,i,b_entry[i]);
	};
	
	printf("\\begin{pmatrix}\n");
	
	
	for (int i=0;i<3;i++){
		printf("%.3g\\\\\n",gsl_vector_get(b,i));
	};
	
	printf("\\end{pmatrix}$$\n");
	
	gsl_vector *x = gsl_vector_calloc(3);
	
	gsl_linalg_HH_solve(A,b,x);

	printf("\nThe solution to the matrix equation is:\n");
	
	printf("\n$$ \\vec{x} = \\begin{pmatrix}\n");
	
	for (int i=0;i<3;i++){
		printf("%.3g\\\\\n",gsl_vector_get(x,i));
	};
	
	printf("\\end{pmatrix}$$\n");
	
	gsl_matrix_free(A);
	gsl_vector_free(b);
	gsl_vector_free(x);
	
	return 0;
}
