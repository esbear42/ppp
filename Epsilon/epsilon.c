#include<stdio.h>
#include<math.h>
#include<limits.h>
#include<float.h>

#include"equal.h"

int main(void){

int a = 1;
while(a<a+1){a=a+1;};
printf("The maximum integer found using a while loop is:     %i\n",a);

int b = 1;
do{b=b+1;}
while(b<b+1);
printf("The maximum integer found using a do-while loop is:  %i\n",b);

int c;
for(c = 1; c<c+1;c=c+1);
printf("The maximum integer found using a for loop is:       %i\n",c);

int d = 1;
while(d>d-1){d=d-1;};
printf("The minimum integer found using a while loop is:    %i\n",d);

int e = 1;
do{e=e-1;}
while(e>e-1);
printf("The minimum integer found using a do-while loop is: %i\n",e);

int f;
for(f=1; f>f-1;f=f-1);
printf("The minimum integer found using a for loop is:      %i\n",f);



float g =1;
while(1+g!=1){g/=2;};
g*=2;
printf("\nThe machine epsilon for float is:       %g\n",g);

double h =1.0;
while(1.0+h!=1.0){h/=2;};
h*=2;
printf("The machine epsilon for double is:      %g\n",h);

long double i =1.0;
while(1.0+i!=1.0){i/=2;};
i*=2;
printf("The machine epsilon for long double is: %Lg\n",i);

printf("The encoded values ought to be:\n");
printf("Float:       %g\n",FLT_EPSILON);
printf("Double:      %g\n",DBL_EPSILON);
printf("Long double: %LG\n",LDBL_EPSILON);



//This was as high as I could go and still have a reasonable
//computing speed on a virtualbox setup.
int max = INT_MAX/128;

float j = max;
float sum_down = 0;
do{sum_down=sum_down+1/j;}
while(--j);

float k=1;
float sum_up = 0;
while(k!=max+1){
	sum_up = sum_up+1/k;
	k=k+1.0;
};



double l = max;
double sum_down_d = 0;
do{sum_down_d=sum_down_d+1/l;}
while(--l);

double m=1;
double sum_up_d = 0;
while(m!=max+1){
	sum_up_d = sum_up_d+1/m;
	m=m+1.0;
};

printf("\nUsing float the sum of the inverse of all integers is:\n");
printf("Summing up:   %.15g\n",sum_up);
printf("Summing down: %.15g\n",sum_down);

printf("\nUsing doubles instead yields higher numbers:\n");
printf("Summing up:   %.15g\n",sum_up_d);
printf("Summing down: %.15g\n",sum_down_d);

printf("\nThe actual infinite sum diverges.\n");

printf(" \u221e  1\n");
printf(" \u03a3  - = \u221e\n");
printf("n=1 n\n");

printf("\nSumming up the highest numbers are added first,\n so once n becomes large further parts of the sum \n become irrelevant and are rounded off.\n When summing up the series converges.\n When summing down the problem is lesser,\n and rounding up is the problem.\n Using doubles instead of floats reduces rounding errors significantly.\n");





return 0;
}

